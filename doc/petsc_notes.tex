\documentclass[11pt]{article}

\usepackage[letterpaper, margin=2.5cm]{geometry}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{multirow}
\usepackage{placeins}

\bibliographystyle{plain}

\title{Notes on PETSc}
\author{Aditya Kashi}

\begin{document}
\maketitle

\begin{abstract}
This is a documentation of the PETSc internal structures that might be useful in implementing custom preconditioners. It is mainly related to direct access of data contained in \texttt{Mat} and \texttt{Vec} objects. It is not meant to be exhaustive. Some general points are also discussed.
\end{abstract}

\section{Data management}

\subsection{\texttt{PetscLayout}}
\label{sec:layout}
``Defines layout of vectors and matrices across processes (which rows are owned by which processes)", according to the source code. Contains the following (among others); note that as used in \texttt{Mat}. `Thing's below can be either rows or columns.
\begin{itemize}
\item \texttt{n} is the number of things stored locally (`local things' below)
\item \texttt{N} is the total global number of things
\item \texttt{rstart} is the global index of the first local thing
\item \texttt{rend} is 1 plus the global index of the last local thing
\item \texttt{bs} is the size of a dense block in case of a block matrix; the total number of entries in a block is then \texttt{bs\^2}
\end{itemize}
[Location: PETSC\_DIR/include/petscis.h; see \texttt{\_n\_PetscLayout}]

\section{Matrix}

\subsection{\texttt{Mat}}
\label{sec:mat}
This is the main matrix object. It's a typedef of \texttt{\_p\_mat}. 

Its main member is \texttt{data} of type \texttt{void*}. It can point to any of PETSc's matrix implementations, the ones of interest to us being \texttt{Mat\_SeqAIJ} and \texttt{Mat\_MPIAIJ}. Note that this pointer points to a \texttt{Mat\_SeqAIJ} whenever the program is run with a single process, even if the matrix has been declared with \texttt{PETSC\_COMM\_WORLD} and not \texttt{PETSC\_COMM\_SELF}. \texttt{data} can also point to one of the block-matrix types; see the next section.

Other useful members are \texttt{rmap} and \texttt{cmap}, both of type \texttt{PetscLayout} described before. From these, one can obtain the number of local rows and columns, global rows and columns, and the local range of rows and columns in global indexing.

[Location: PETSC\_DIR/include/petsc/private/matimpl.h]

\subsection{\texttt{Mat\_SeqAIJ}}
This is a matrix stored on a single processor. It implements a compressed sparse row (CSR) storage. Its useful members are as follows.
\begin{itemize}
\item \texttt{PetscScalar* a} the array containing the non-zero entries
\item \texttt{PetscInt* j} the array containing the column indices of the non-zero entries; has same size as \texttt{a}.
\item \texttt{PetscInt* i} array of size one more than the number of rows. The last entry contains the number of non-zeros in the matrix; the rest contain, for a row, the index of \texttt{a} at which the first entry of the row is stored.
\item \texttt{PetscInt* diag} an array of size number of rows, containing the locations of diagonal entries in \texttt{a} and \texttt{j} for each row.
\end{itemize}

[Location: PETSC\_DIR/src/mat/impls/aij/seq/aij.h]

\subsection{\texttt{Mat\_MPIAIJ}}
\label{subsec:mpiaij}
\texttt{Mat\_MPIAIJ} is a distributed matrix. How the distribution is handled has not been investigated, but one the matrix is fully assembled, one can access the locally stored rows of the matrix using the structures described here. A parallel matrix is stored with certain number of rows (and all column corresponding to these rows) on each processor \cite{petsc-user-ref}. That is, the matrix is partitioned into rows. Note that the term `diagonal block' below means the square sparse block corresponding to coupling terms between different unknowns belonging to the current process. Similarly, `off-diagonal block' means the rectangular sparse block corresponding to coupling terms between unknowns belonging to the current process and unknowns belonging to other processes.
\begin{itemize}
\item \texttt{A} It is a \texttt{Mat} object containing the diagonal square block of all local rows of the matrix. Its \texttt{data} member (section \ref{sec:mat}) is generally of type \texttt{Mat\_SeqAIJ}.
\item \texttt{B} It is a \texttt{Mat} object containing the non-square non-diagonal block of all local rows of the matrix. Its \texttt{data} member (section \ref{sec:mat}) is generally of type \texttt{Mat\_SeqAIJ} too.
\end{itemize}
The entries of the matrix etc. can be accessed through \texttt{A} and \texttt{B} described above.

[Location: PETSC\_DIR/src/mat/impls/aij/mpi/mpiaij.h]

\section{Block Matrices}
By `block matrix', a sparse matrix made up of small dense blocks is implied. This is different from the `block' used in the context of MPI blocks \ref{subsec:mpiaij}. A \texttt{Mat}'s \texttt{data} member may point to an object of one of the types below.

\subsection{\texttt{Mat\_SeqBAIJ}}
This is a single-processor block matrix type. Its useful members are as follows.
\begin{itemize}
	\item \texttt{PetscScalar* a} the array containing the non-zero entries. \emph{Important note}: while the blocks are stored in block-row major format, within blocks, the scalars are arranged in column-major format.
	\item \texttt{PetscInt* j} the array containing the block-column indices of the non-zero blocks; does NOT have the same size as \texttt{a}.
	\item \texttt{PetscInt* i} array of size one more than the number of block-rows. The last entry contains the number of non-zero blocks in the matrix; the rest contain, for a block-row, the index of \texttt{a} at which the first entry of the block-row is stored.
	\item \texttt{PetscInt mbs} number of block-rows in the matrix.
	\item \texttt{PetscInt nbs} number of block-columns in the matrix.
	\item  \texttt{PetscInt* diag} an array of size equal to the number of block-rows; it contains the locations of diagonal blocks in \texttt{a} and \texttt{j} for each block-row.
\end{itemize}
The \texttt{PetscLayout} (sec. \ref{sec:layout}) object \texttt{rmap} for this type contains data corresponding to \emph{rows} of the matrix, NOT block-rows.

[Location: PETSC\_DIR/src/mat/impls/baij/seq/baij.h]

\subsection{\texttt{Mat\_MPIBAIJ}}
\label{subsec:mpibaij}
\texttt{Mat\_MPIAIJ} is a distributed block matrix. A parallel matrix is stored with certain number of block-rows (and all block-columns corresponding to these rows) on each processor \cite{petsc-user-ref}. Note that the term `diagonal block' below means the square sparse block corresponding to coupling terms between different unknowns belonging to the current process. Similarly, `off-diagonal block' means the rectangular sparse block corresponding to coupling terms between unknowns belonging to the current process and unknowns belonging to other processes.
\begin{itemize}
	\item \texttt{A} It is a \texttt{Mat} object containing the diagonal square block of all local block-rows of the matrix. Its \texttt{data} member (section \ref{sec:mat}) is generally of type \texttt{Mat\_SeqBAIJ}.
	\item \texttt{B} It is a \texttt{Mat} object containing the non-square non-diagonal block of all local block-rows of the matrix. Its \texttt{data} member (section \ref{sec:mat}) is generally of type \texttt{Mat\_SeqBAIJ} too.
\end{itemize}
The entries of the matrix etc. can be accessed through \texttt{A} and \texttt{B} described above.

\subsection{\texttt{DMDA}}
After calling \texttt{DMDACreate3d} or similar, \texttt{DMSetUp} \emph{must} be called PETSc 3.8 onwards.

\subsection{Miscellaneous notes}
We did not need to access entries of \texttt{Vec}s by direct access; \texttt{VecGetArray} does the job.

\section{PETSc preconditioners}

\subsection{Global preconditioners}
To extract local \texttt{KSP}s from a global preconditioner such as additive Schwarz or block Jacobi, \texttt{KSPSetOperators} must be called before \texttt{KSPSetUp}, which is needed for calling \texttt{PCASMGetSubKSPs} or similar.

\bibliography{refs}
\end{document}