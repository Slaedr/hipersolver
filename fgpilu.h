/** \file fgpilu.h
 * \brief Header for fine-grained asynchronous point ILU preconditioner of E. Chow and A. Patel.
 * \author Aditya Kashi
 */

#ifndef __FGPILU_H
#define __FGPILU_H

#include <stdbool.h>
#include <petscpc.h>

#ifdef __cplusplus
extern "C" {
#endif

/// State necessary for Chow-Patel ILU factorization
/** NOTE: when an object of this struct is first created, \ref setup needs to be initialized to "false".
 * L and U is stored in CSR (compressed sparse row) format
 *
 * All arrays here, except scale, are allocated and initialized by the compute_fgpilu routine the first time it's run.
 * In particular, L and U are initialized once with the initial guess for the FGPILU factorization.
 * However, note that the arrays need to be freed using the cleanup routine.
 *
 * \note cputime and walltime should be set to zero initially by the application program
 */
typedef struct
{
	int nbuildsweeps;		///< Number of asynchronous sweeps to apply to build the LU factorization
	int napplysweeps;		///< Number of iterations (sweeps) to perform in each application of the preconditioner

	PetscInt nrows;			///< Number of rows in the local matrix

	PetscReal* luvals;		///< Nonzero values of the ILU factorization of the diagonal block of the local part of the prec matrix
	PetscInt* lucols;		///< Column indices of the ILU factorization of the diagonal block of the local part of the prec matrix
	
	/// Row pointers into value- and column-index-arrays 
	/// of the ILU factorization of the diagonal block of the local part of the prec matrix
	PetscInt* lurowp;

	/// Indices of diagonal entries in value- and column-index-arrays 
	/// of the ILU factorization of the diagonal block of the local part of the prec matrix
	PetscInt* ludiagind;	

	PetscReal* scale;		///< Dense vector used to symmetrically scale the LHS matrix before factorization

	PetscReal* y;			///< Intermediate array used in the solve routine

	bool setup;				///< Stores whether or not arrays have been allocated 

	double cputime;			///< Total CPU time taken by FGPILU
	double walltime;		///< Total wall-clock time taken by FGPILU
	double factorcputime;	///< CPU time taken for factorization
	double factorwalltime;	///< Wall-clock time for factorization
	double applycputime;
	double applywalltime;
} H_ILU_data;

/// Set up the preconditioner context for use with FGPILU
/** Sets control data in the [control structure](@ref H_ILU_Data) and
 * calls [the function](@ref compute_fgpilu_local) to compute the local ILU factors.
 * \param[in] nbsweeps Number of async ILU sweeps to use to build the factors
 * \param[in] nasweeps Number of async Gauss-Seidel sweeps to apply the preconditioner
 * \param[in|out] pc The PETSc preconditioner context
 */
void setup_fgpilu(const int nbsweeps, const int nasweeps, PC pc);

/// Free arrays in the context struct
void cleanup_fgpilu(PC pc);

/// Computes the local L and U factors (ILU0) in parallel using OpenMP
/** \param pc is the PETSc preconditioner context.
 *
 * This version stores the L and U factors separately from the preconditioning operator M.
 * This way, L and U can provide initial values for the factorization.
 * We assume that the PC object's context struct, H_ILU_data, contains initial values of L and U.
 *
 * When this routine is run for the first time, memory is allocated to all arrays in H_ILU_data
 * and the matrix lu is populated with values copied from the preconditioning matrix M.
 */
PetscErrorCode compute_fgpilu_local(PC pc);

/// Applies the preconditioner by Jacobi iterations, as done by Anzt, Chow and Dongarra [2015], in parallel.
/** \param pc is the PETSc preconditioner context
 * \param r is the residual vector, ie, the RHS
 * \param z is the unknown vector to be computed.
 *
 * NOTE: It is assumed that the length of r and z on the local process is the same.
 */
PetscErrorCode apply_fgpilu_jacobi_local(PC pc, Vec r, Vec z);

/// Solver routine for stand-alone FGPILU solver
/** Compute the [factorization](@ref compute_fgpilu_local) before calling this.
 */
PetscErrorCode solveFGPILU(PC pc, Vec b, Vec x, Vec r, PetscReal rtol, PetscReal abstol, PetscReal dtol, PetscInt maxits);

#ifdef __cplusplus
}
#endif

#endif
