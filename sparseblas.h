/** \file sparseblas.h
 * \brief Some sparse operations required in the solvers
 * \author Aditya Kashi
 */

#ifndef __SPARSEBLAS_H
#define __SPARSEBLAS_H

#include <petscsys.h>

#ifdef __cplusplus
extern "C" {
#endif

/// search through inner indices
/** Finds the position in the index arary that has value indtofind
 * Searches between positions
 * \param[in] start, and
 * \param[in] end
 */
/* Modify for performance: Instead of passing the entire array in aind, pass a pointer to
 * the starting entry, then reverse the loop so that j goes from end to 0.
 * That way, the loop termination check is j >= 0 which should be faster than j < end.
 */
static inline void inner_search(const PetscInt *const aind, const PetscInt start, const PetscInt end, const PetscInt indtofind, PetscInt *const pos)
{
	for(PetscInt j = start; j < end; j++) {
		if(aind[j] == indtofind) {
			*pos = j;
			break;
		}
	}
}

//void inner_search(const PetscInt *const aind, const PetscInt start, const PetscInt end, const PetscInt indtofind, PetscInt* pos);

/// Computes the dot product of (sub-vectors of) two sparse vectors serially
/** A search is used in the inner loop to locate the needed values in input b.
 * \param[in] maxind The dot product computed is A[:maxind]^T B[:maxind] (maxind is not included in the range)
 * \param[in|out] dotval is not initialized in any way; the dot product is added to the value pointed to by it.
 */
void ddot(const PetscReal *const avals, const PetscInt *const aind, const PetscInt annz, 
		const PetscReal *const bvals, const PetscInt *const bind, const PetscInt bnnz, 
		const PetscInt maxind, PetscReal *const dotval);

/// Computes dot product of a row of CSR matrix A and a column of CSR matrix B
/** \param[in] maxind is the maximum inner index upto (but not including) which the dot product is computed.
 */
PetscReal ddot_rowcol_csrcsr(const PetscReal *const avals, const PetscInt *const acols, const PetscInt *const arowp,
		const PetscReal *const bvals, const PetscInt *const bcols, const PetscInt *const browp, 
		const PetscInt i, const PetscInt j, const PetscInt maxind);

/// Symmetric diagonal scaling of a CSR matrix stored in "Ellpack" format
/** The left and right diagonal scaling matrices are stored as two dense vectors.
 * \param[in] dl left scaling "matrix"
 * \param[in] nl length of left vector dl
 * \param[in] dr right scaling matrix
 * \param[in] nr length of right scaling vector dr
 * \param[in|out] Avals Avals[i][j] contains the element at the Acolinds[i][j] column of the ith row of the matrix A to be scaled
 * \param[in] Acolinds contains column indices corresponding to the entries in Avals
 * \param[in] Annz contains the number of non-zeros in each row of the matrix A
 *
 * Note that it's assumed that A is an nl x nr matrix.
 */
void symscale_csrellpack(const PetscReal *const dl, const PetscInt nl, const PetscReal *const dr, const PetscInt nr,
		PetscReal *const *const Avals, const PetscInt *const *const Acolinds, const PetscInt *const Annz);

/// y := aAx + by
/** Note that this operation differs from the equivalent MKL routine.
 * \param[in] A sparse CSR matrix 
 * \param[in] x dense vector
 * \param[in] a scalar
 * \param[in] b scalar
 * \param[in|out] y dense vector
 */
void dcsrgemv(const PetscReal *const Avals, const PetscInt *const Acols, const PetscInt *const Arowp, const PetscInt nrows, const PetscReal a,
		const PetscReal *const x, const PetscReal b, PetscReal *const y);



#ifdef __cplusplus
}
#endif

#endif
