#include "sparseblas.h"

void ddot(const PetscReal *const restrict avals, const PetscInt *const restrict aind, const PetscInt annz, 
		const PetscReal *const restrict bvals, const PetscInt *const restrict bind, const PetscInt bnnz, 
		const PetscInt maxind, PetscReal *const restrict dotval)
{
	PetscInt k = 0, l = 0;
	while(aind[k] < maxind && k < annz)
	{
		// linear search through indices of vector b, but with some optimizations assuming the indices are sorted
		// should probably replace with binary search
		
		// only search starting from last value of l
		for(; l < bnnz; l++) {
			if(bind[l]==aind[k]) {
				(*dotval) += avals[k]*bvals[l];
				l++;
				break;
			}
			else if(bind[l] > aind[k])
				break;
		}
		k++;
	}
}

// NOTE: we might need omp atomic reads for the avals[k]
PetscReal ddot_rowcol_csrcsr(const PetscReal *const restrict avals, const PetscInt *const restrict acols, const PetscInt *const restrict arowp,
		const PetscReal *const restrict bvals, const PetscInt *const restrict bcols, const PetscInt *const restrict browp, 
		const PetscInt i, const PetscInt j, const PetscInt maxind)
{
	PetscReal dotval = 0;
	for(PetscInt k = arowp[i]; acols[k] < maxind && k < arowp[i+1]; k++)
	{
		for(PetscInt l = browp[acols[k]]; l < browp[acols[k]+1]; l++) {
			if(bcols[l] == j) {
				dotval += avals[k]*bvals[l];
				break;
			}
		}
	}
	return dotval;
}

/// TODO: Experiment with and without restrict on dl and dr
void symscale_csrellpack(const PetscReal *const dl, const PetscInt nl, const PetscReal *const dr, const PetscInt nr,
		PetscReal *const *const restrict Avals, const PetscInt *const *const restrict Acolinds, const PetscInt *const restrict Annz)
{
	// loop over rows
	for(int i = 0; i < nl; i++)
	{
		for(int jz = 0; jz < Annz[i]; jz++)
		{
#ifdef DEBUG
			if(Acolinds[i][jz] >= nr)
				printf("! symscale_csr: Indices in Acolinds exceed size of right diagonal matrix!\n");
#endif
			Avals[i][jz] = Avals[i][jz] * dl[i] * dr[Acolinds[i][jz]];
		}
	}
}

void dcsrgemv(const PetscReal *const restrict Avals, const PetscInt *const restrict Acols, const PetscInt *const restrict Arowp, const PetscInt nrows, const PetscReal alpha,
		const PetscReal *const restrict x, const PetscReal beta, PetscReal *const restrict y)
{
	for(PetscInt i = 0; i < nrows; i++)
	{
		for(PetscInt ji = Arowp[i]; ji < Arowp[i+1]; ji++)
		{
			y[i] = beta*y[i] + alpha*Avals[ji]*x[Acols[ji]];
		}
	}
}

/*PetscInt inner_search(const PetscInt *const aind, const PetscInt start, const PetscInt end, const PetscInt indtofind)
{
	for(PetscInt j = start; j < end; j++)
	{
		if(aind[j] == indtofind)
			return j;
	}
	return -1;
}*/
