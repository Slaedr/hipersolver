PHiPerSolver
============

A suite of preconditioners and solvers for solving large sparse linear systems on modern processors. It is being designed to be interoperable with the PETSc library.
