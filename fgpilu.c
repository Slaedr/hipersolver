/** \file fgpilu.c
 * \brief Common routines for the Chow-Patel fine-grained parallel ILU factorization.
 * \author Aditya Kashi
 */

#include "fgpilu.h"

void setup_fgpilu(const int nbsweeps, const int nasweeps, PC pc)
{
	PCSetType(pc, PCSHELL);
	
	// set control structure
	H_ILU_data ctx;
	ctx.nbuildsweeps = nbsweeps;
	ctx.napplysweeps = nasweeps;
	ctx.setup = false;
	ctx.cputime = 0;
	ctx.walltime = 0;
	PCShellSetContext(pc, &ctx);

	PCShellSetApply(pc, &apply_fgpilu_jacobi_local);
	PCShellSetName(pc, "FGPILU");

	// compute ILU factors and store
	compute_fgpilu_local(pc);
}

void cleanup_fgpilu(PC pc)
{
	H_ILU_data* ctx;
	PCShellGetContext(pc, (void**)&ctx);
	if(ctx->setup == true) {
		free(ctx->lurowp);
		free(ctx->lucols);
		free(ctx->luvals);
		free(ctx->ludiagind);
		free(ctx->scale);
		free(ctx->y);
		ctx->setup = false;
	}
#ifdef DEBUG
	printf("  cleanup_fgpilu(): Internal arrays deleted.\n");
#endif
}
