# Makefile for Hipersolvers project
# Aditya Kashi
#
# Make sure environment variables CC and CXX have been set with the compilers to use.
# Make sure PETSC_DIR and PETSC_ARCH have been set.
# Set DEBUG while/before calling make for a debug build
# Set USE_OMP for compiling with OpenMP in a debug build; release builds are always with OpenMP.
# Set OPENBLAS_INCLUDE_DIR as the path where Openblas include files are present
#  and OPENBLAS_LIB_DIR for the location of libopenblas.so.
# Set USE_MKL to compile with Intel MKL
# Set BUILD_KNC = 1 to compile for the Knights Corner (MIC) accelerator.

NAME := hipersolver
PREFIX := build

INCLUDES := 
CUSERDEFS := 
CFLAGS := -Wall
LFLAGS := 
LDFLAGS := 
ifeq (${BUILD_KNC}, 1)
  CFLAGS := ${CFLAGS} -mmic
  LDFLAGS := ${LDFLAGS} -mmic
endif

# set BLAS library to use
ifdef OPENBLAS_INCLUDE_DIR
  BLAS_LIB := -L${OPENBLAS_LIB_DIR} -lopenblas
  CUSERDEFS := ${CUSERDEFS} -DUSING_OPENBLAS
  INCLUDES := ${INCLUDES} -I${OPENBLAS_INCLUDE_DIR}
else ifdef USE_MKL
  ifeq (${BUILD_KNC}, 1)
	BLAS_LIB := -L${MKLROOT}/lib/mic -lmkl_rt -lpthread -lm -ldl
  else
    BLAS_LIB := -L${MKLROOT}/lib/intel64 -lmkl_rt -lpthread -lm -ldl
  endif
  CUSERDEFS := ${CUSERDEFS} -DUSING_MKL
  INCLUDES := ${INCLUDES} -I${MKLROOT}/include
endif

#Profile using gprof
PROFILE= #-pg

ifeq (${CC}, ${filter ${CC}, gcc mpicc cc clang})
 CFLAGS := ${CFLAGS} -Winline -ftree-vectorizer-verbose=3 -std=c11
else ifeq (${CC}, ${filter ${CC}, icc mpiicc})
 CFLAGS := ${CFLAGS} -Winline -qopt-report=2 -std=c11
else ifeq ($(CC),pgcc)
 $(info "Setting flags for pgcc")
 CFLAGS := ${CFLAGS} -std=c11 #-Minfo=vect -Minfo=inline
endif

# if DEBUG is not defined, code is compiled in debug mode. Otherwise, optimizations are enabled
ifndef DEBUG

  $(info "Compiling with optimizations")
  ifeq (${CC}, ${filter ${CC}, gcc mpicc cc clang})
	CFLAGS := ${CFLAGS} -O3 -fopenmp
  else ifeq (${CC}, ${filter ${CC}, icc mpiicc})
	CFLAGS := ${CFLAGS} -O3 -qopenmp
  else ifeq ($(CC),pgcc)
    $(info "Setting flags for pgcc")
	CFLAGS := ${CFLAGS} -O3 -Msafeptr=all -fast -mp #-Minfo=vect -Minfo=inline
  endif

else

  PROFILE := -pg
  $(info "Compiling debug version")
  CFLAGS := ${CFLAGS} -ggdb
  ifdef USE_OMP
    $(info "Setting flags for OpenMP")
    ifeq (${CC}, ${filter ${CC}, gcc mpicc cc clang})
      CFLAGS := ${CFLAGS} -fopenmp
    else ifeq (${CC}, ${filter ${CC}, icc mpiicc})
      CFLAGS := ${CFLAGS} -qopenmp
    else ifeq ($(CC),pgcc)
      CFLAGS := ${CFLAGS} -mp
    endif
  endif
  CUSERDEFS := ${CUSERDEFS} -DDEBUG
  LFLAGS := ${LFLAGS} -ggdb #-lmkl_intel_lp64 -lmkl_intel_thread -liomp5 -lmkl_core -lpthread

endif

LFLAGS := ${LFLAGS} -fPIC
LIBS = -lm -L${PETSC_DIR}/${PETSC_ARCH} -lpetsc ${BLAS_LIB}
INCLUDES := ${INCLUDES} -I${PETSC_DIR}/include -I${PETSC_DIR}/${PETSC_ARCH}/include

clibsrcs =$(wildcard *.c)
clibobjst =$(clibsrcs:.c=.o)
clibobjs = $(foreach obj,$(clibobjst),$(PREFIX)/$(obj))

${NAME}: ${clibobjs}
	${CC} ${LDFLAGS} -shared -o ${PREFIX}/lib${NAME}.so $^

$(PREFIX)/%.o: %.c
	${CC}  ${CFLAGS} ${INCLUDES} ${LIBS} ${PROFILE} ${LFLAGS} ${CUSERDEFS} -c -o $@ $<

#.PHONY : clean
clean:
	rm -f $(clibobjs) ${PREFIX}/lib${NAME}.so ${PREFIX}/*.optrpt

