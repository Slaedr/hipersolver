/** \file fgpilu_omp.c
 * \brief Implementation of the Chow-Patel fine-grained parallel ILU routine(s) using OpenMP.
 * \author Aditya Kashi
 */

#include "fgpilu.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>

#ifdef USING_MKL
#include <mkl_cblas.h>
#endif
#ifdef USING_OPENBLAS
#include <cblas.h>
#endif

#include <../src/mat/impls/aij/mpi/mpiaij.h>
#include "sparseblas.h"

#define THREAD_CHUNK_SIZE 1024

PetscErrorCode compute_fgpilu_local(PC pc)
{
	PetscErrorCode ierr = 0;
	PetscInt firstrow, lastrow, localrows, localcols, globalrows, globalcols, numprocs, thisrank;
	MPI_Comm_size(PETSC_COMM_WORLD,&numprocs);
	MPI_Comm_rank(PETSC_COMM_WORLD,&thisrank);
	
	// get control structure
	H_ILU_data* ctx;
	PCShellGetContext(pc, (void**)&ctx);

	// get the local preconditioning matrix - we operate on the diagonal matrix block corresponding to this process
	
	Mat A;
	PCGetOperators(pc, NULL, &A);
	MatGetOwnershipRange(A, &firstrow, &lastrow);		// assuming Petsc distributes matrices by row, ie. all entries in a given row reside on the same processor
	MatGetLocalSize(A, &localrows, &localcols);
	ctx->nrows = localrows;
	MatGetSize(A, &globalrows, &globalcols);
	const Mat_SeqAIJ *Adiag;
	//const Mat_SeqAIJ *Aoffdiag;

	// get access to local matrix entries

	if(numprocs > 1) {	
		const Mat_MPIAIJ *Atemp = (const Mat_MPIAIJ*)A->data;
		Adiag = (const Mat_SeqAIJ*)Atemp->A->data;						// Diagonal part of the local part of the matrix
		//Aoffdiag = (const Mat_SeqAIJ*)Atemp->B->data;					// Off-diagonal part of the local part of the matrix
	}
	else {
		Adiag = (const Mat_SeqAIJ*)A->data;
		//Aoffdiag = NULL;						// for 1 process, everything is in Adiag
	}

	const PetscInt* Adrowp = Adiag->i;
	const PetscInt* Adcols = Adiag->j;
	const PetscInt* Addiagind = Adiag->diag;
	const PetscReal* Advals = Adiag->a;
	const PetscInt Adnnz = Adiag->nz;

#ifdef DEBUG
	if(thisrank==0)
		printf("compute_fgpilu_local(): firstrow = %d, lastrow = %d, localrows = %d, localcols = %d, globalrows = %d, globalcols = %d\n", 
			firstrow, lastrow, localrows, localcols, globalrows, globalcols);
	if(localrows != localcols)
		printf("! compute_fgpilu_local(): Local matrix is not square! Size is %dx%d.\n", localrows, localcols);
	if(localrows != (lastrow-firstrow))
		printf("! compute_fgpilu_local(): Ownership range %d and local rows %d are not consistent with each other!", 
				lastrow-firstrow, localrows);
	if(Adnnz != Adrowp[localrows])
		printf("! compute_fgpilu_local(): M nnz = %d, last entry of M rowp = %d!\n", Adnnz, Adrowp[localrows]);
	if(localrows != ctx->nrows)
		printf("! compute_fgpilu_local(): The number of rows has changed!\n");
#endif

	// If this is the first time that this routine is being run, allocate arrays in ctx and initialize L and U
	
	if(ctx->setup == false)
	{
		if(thisrank==0)
			printf("compute_fgpilu_local(): First-time setup\n");

		// Allocate lu
		
		ctx->lurowp = (PetscInt*)malloc((localrows+1)*sizeof(PetscInt));
		ctx->lucols = (PetscInt*)malloc(Adnnz*sizeof(PetscInt));
		ctx->luvals = (PetscReal*)malloc(Adnnz*sizeof(PetscReal));
		ctx->ludiagind = (PetscInt*)malloc(localrows*sizeof(PetscInt));
		for(int i = 0; i < localrows+1; i++)
			ctx->lurowp[i] = Adrowp[i];
		for(int j = 0; j < Adnnz; j++) {
			ctx->lucols[j] = Adcols[j];
			ctx->luvals[j] = Advals[j];
		}
		for(int i = 0; i < localrows; i++)
			ctx->ludiagind[i] = Addiagind[i];

		// intermediate array for the solve part
		ctx->y = (PetscReal*)malloc(ctx->nrows*sizeof(PetscReal));
		for(int i = 0; i < ctx->nrows; i++)
			ctx->y[i] = 0;
		
		ctx->scale = (PetscReal*)malloc(localrows*sizeof(PetscReal));
		
		ctx->setup = true;

	}
	
	PetscInt *lurowp = ctx->lurowp, *lucols = ctx->lucols, *ludiagind = ctx->ludiagind;
	PetscReal *luvals = ctx->luvals;

	struct timeval time1, time2;
	gettimeofday(&time1, NULL);
	double initialwtime = (double)time1.tv_sec + (double)time1.tv_usec * 1.0e-6;
	double initialctime = (double)clock() / (double)CLOCKS_PER_SEC;

	// get the diagonal scaling matrix
	
#pragma omp parallel for simd default(shared)
	for(int i = 0; i < localrows; i++)
		ctx->scale[i] = 1.0/sqrt(Advals[Addiagind[i]]);
		//ctx->scale[i] = 1.0;

	// compute L and U
	if(thisrank==0)
		printf("compute_fgpilu_local: Factorizing. %d build sweeps, chunk size is %d.\n", 
			ctx->nbuildsweeps, THREAD_CHUNK_SIZE);
	
	for(int isweep = 0; isweep < ctx->nbuildsweeps; isweep++)
	{
#pragma omp parallel for default(shared) schedule(dynamic, THREAD_CHUNK_SIZE)
		for(PetscInt irow = 0; irow < localrows; irow++)
		{
			for(PetscInt j = Adrowp[irow]; j < Adrowp[irow+1]; j++)
			{
				if(irow > lucols[j])
				{
					PetscReal sum = ctx->scale[irow] * Advals[j] * ctx->scale[Adcols[j]];

					for(PetscInt k = lurowp[irow]; (k < lurowp[irow+1]) && (lucols[k] < lucols[j]); k++) 
					{
						PetscInt pos = -1;
						inner_search(lucols, ludiagind[lucols[k]], lurowp[lucols[k]+1], lucols[j], &pos);
						if(pos == -1) {
							continue;
						}
						sum -= luvals[k]*luvals[pos];
					}

					sum = sum / luvals[ludiagind[lucols[j]]];
					luvals[j] = sum;
				}
				else
				{
					// compute u_ij
					PetscReal sum = 0;

					for(PetscInt k = lurowp[irow]; (k < lurowp[irow+1]) && (lucols[k] < irow); k++) 
					{
						PetscInt pos = -1;
						// search for column index lucols[j], 
						// between the diagonal index of row lucols[k] and the last index of row lucols[k]
						inner_search(lucols, ludiagind[lucols[k]], lurowp[lucols[k]+1], lucols[j], &pos);
						if(pos == -1) {
							continue;
						}
						sum += luvals[k]*luvals[pos];
					}

					luvals[j] = ctx->scale[irow] * Advals[j] * ctx->scale[Adcols[j]] - sum;
				}
			}
		}
	}
	
	gettimeofday(&time2, NULL);
	double finalwtime = (double)time2.tv_sec + (double)time2.tv_usec * 1.0e-6;
	ctx->factorwalltime += (finalwtime - initialwtime);
	double finalctime = (double)clock() / (double)CLOCKS_PER_SEC;
	ctx->factorcputime += (finalctime - initialctime);

	return ierr;
}

PetscErrorCode apply_fgpilu_jacobi_local(PC pc, Vec r, Vec z)
{
	// get local arrays
	const PetscReal *ra;
	PetscReal *za;
	PetscInt start, end;
	VecGetOwnershipRange(r, &start, &end);
	H_ILU_data* ctx;
	PCShellGetContext(pc, (void**)&ctx);
#ifdef DEBUG
	if(ctx->nrows != end-start) {
		printf("! apply_fgpilu_jacobi_local: Dimension of the input vector r does not match dimension of the preconditioning matrix!");
		return -1;
	}
#endif
	
	PetscInt *lurowp = ctx->lurowp, *lucols = ctx->lucols, *ludiagind = ctx->ludiagind;
	PetscReal *luvals = ctx->luvals;
	
	VecGetArray(z, &za);
	VecGetArrayRead(r, &ra);
	
	struct timeval time1, time2;
	gettimeofday(&time1, NULL);
	double initialwtime = (double)time1.tv_sec + (double)time1.tv_usec * 1.0e-6;
	double initialctime = (double)clock() / (double)CLOCKS_PER_SEC;

	// initially, z := Sr
#pragma omp parallel for simd default(shared)
	for(int i = 0; i < ctx->nrows; i++) {
		za[i] = ctx->scale[i]*ra[i];
	}
	
	/** solves Ly = Sr by asynchronous Jacobi iterations.
	 * Note that if done serially, this is a Gauss-Siedel loop, ie., a forward-substitution.
	 */
	for(int isweep = 0; isweep < ctx->napplysweeps; isweep++)
	{
#pragma omp parallel for default(shared) schedule(dynamic, THREAD_CHUNK_SIZE)
		for(PetscInt i = 0; i < ctx->nrows; i++)
		{
			PetscReal sum = 0;
			for(PetscInt j = lurowp[i]; j < ludiagind[i]; j++)
			{
				sum += luvals[j] * ctx->y[lucols[j]];
			}
			ctx->y[i] = za[i] - sum;
		}
	}

	/* Solves Uz = y by asynchronous Jacobi iteration.
	 * If done serially, this is a back-substitution.
	 */
	for(int isweep = 0; isweep < ctx->napplysweeps; isweep++)
	{
#pragma omp parallel for default(shared) schedule(dynamic, THREAD_CHUNK_SIZE)
		for(PetscInt i = ctx->nrows-1; i >= 0; i--)
		{
			PetscReal sum = 0;
			for(PetscInt j = ludiagind[i]+1; j < lurowp[i+1]; j++)
			{
				sum += luvals[j] * za[lucols[j]];
			}
			za[i] = 1.0/luvals[ludiagind[i]] * (ctx->y[i] - sum);
		}
	}

	// correct z
#pragma omp parallel for simd default(shared)
	for(int i = 0; i < ctx->nrows; i++)
		za[i] = za[i]*ctx->scale[i];

	gettimeofday(&time2, NULL);
	double finalwtime = (double)time2.tv_sec + (double)time2.tv_usec * 1.0e-6;
	ctx->applywalltime += (finalwtime - initialwtime);
	double finalctime = (double)clock() / (double)CLOCKS_PER_SEC;
	ctx->applycputime += (finalctime - initialctime);

	// Restore arrays
	VecRestoreArrayRead(r, &ra);
	VecRestoreArray(z, &za);
	
	return 0;
}

/** \todo PETSc sparse BLAS calls need to be replaced by threaded sparse BLAS.
 */
PetscErrorCode solveFGPILU(PC pc, Vec b, Vec x, Vec r, PetscReal rtol, PetscReal abstol, PetscReal dtol, PetscInt maxits)
{
	PetscReal resnorm = 1, relresnorm = 1, bnorm;
	PetscInt step = 0;
	PetscErrorCode ierr=0;
	
	Vec dx;
	VecDuplicate(x, &dx);

	H_ILU_data* ctx;
	PCShellGetContext(pc, (void**)&ctx);

	// Get original matrix in A
	Mat A;
	PCGetOperators(pc, &A, NULL);

	VecCopy(b, r);
	VecNorm(b, NORM_2, &bnorm);
	
	while(relresnorm > rtol && resnorm > abstol && step < maxits)
	{
		ierr = apply_fgpilu_jacobi_local(pc, r, dx);
		VecAXPY(x, 1.0, dx);
		MatMult(A, x, r);
		VecAYPX(r, -1.0, b);
		VecNorm(r, NORM_2, &resnorm);
		relresnorm = resnorm/bnorm;
		step++;
	}
	return ierr;
}
